# karlender

A bloated calender software.

## Features

-   flexible condition-based recurring tasks
-   automatic scheduling of tasks, based on priority
-   blazing fast and highly configurable 🚀🚀
-   undocumented

## Components

-   `karld` daemon process calculating occurences and scheduling
-   `karlc` cli-interface for karld (uses unix domain socket)
-   `karlgui` graphical user interface made with egui (uses the unix domain socket too)
-   `karlcommon` rust library for shared e.g. protocol
-   _`????`_ browser interface (future, will use websocket)

## Protocol

Packets with JSON-serialized instances of either `ServerboundPacket` or `ClientboundPacket` respectively as declared in ([karlcommon/protocol.d.ts](./karlcommon/protocol.d.ts)) are sent over one of the supported methods.

| Transport          | Note                       | Default location/address     |
| ------------------ | -------------------------- | ---------------------------- |
| Unix domain socket | Every line is a packet.    | `/run/user/<uid>/karlender`. |
| Websocket          | Packets are text messages. | `127.0.0.1:18752`.           |
| TCP                | Every line is a packet     | `127.0.0.1:18751`.           |
| Stdio              | Every line is a packet     | stdin / stdout               |

## Licence

GNU Affero General Public License Version 3 only. See [COPYING](./COPYING)
