use std::ops::Range;

// use crate::condition::{ConditionFind, Direction, Edge};
// use chrono::NaiveDateTime;
// use karlcommon::{Schedule, Task};
// use std::{collections::HashMap, ops::Range};

// pub struct DiscreteCache {
//     tasks: HashMap<u64, DiscreteCacheTask>,
// }
// pub struct DiscreteCacheTask {
//     inner: Task,
//     cached: Option<Range<NaiveDateTime>>,
//     cache: Vec<Range<NaiveDateTime>>,
// }

// impl DiscreteCache {
//     pub fn new_for(tasks: HashMap<u64, Task>) -> Self {
//         Self {
//             tasks: HashMap::from_iter(
//                 tasks
//                     .into_iter()
//                     .map(|(k, v)| (k, DiscreteCacheTask::new(v))),
//             ),
//         }
//     }
// }

// impl DiscreteCacheTask {
//     pub fn new(inner: Task) -> Self {
//         Self {
//             inner,
//             cached: None,
//             cache: vec![],
//         }
//     }

//     pub fn find(&mut self, from: NaiveDateTime, dir: Direction) -> Range<Option<NaiveDateTime>> {
//         // TODO cache
//         // if let Some(c) = self.cached {}
//         return self.find_uncached(from, dir);
//     }

//     pub fn find_uncached(
//         &mut self,
//         from: NaiveDateTime,
//         dir: Direction,
//     ) -> Range<Option<NaiveDateTime>> {
//         assert_eq!(dir, Direction::Forward); // TODO undefined behaviour if dir is not forward (maybe it even works backward)
//         match &self.inner.schedule {
//             Schedule::Condition(o) => {
//                 let start = o.find(Edge::Start, dir, from);
//                 let end = o.find(Edge::End, dir, from);
//                 match (start, end) {
//                     (Some(start), Some(end)) => {
//                         if end < start {
//                             if let Some(start) = o.find(Edge::Start, dir.invert(), from) {
//                                 assert!(start < end);
//                                 Some(start)..Some(end)
//                             } else {
//                                 None..Some(end)
//                             }
//                         } else {
//                             Some(start)..Some(end)
//                         }
//                     }
//                     (None, Some(end)) => {
//                         if let Some(start) = o.find(Edge::Start, dir.invert(), from) {
//                             assert!(start < end);
//                             Some(start)..Some(end)
//                         } else {
//                             None..Some(end)
//                         }
//                     }
//                     (Some(start), None) => Some(start)..None,
//                     (None, None) => None..None,
//                 }
//             }
//             Schedule::Never => None..None,
//             Schedule::Static(_) => None..None,      // TODO
//             Schedule::Dynamic { .. } => None..None, // TODO
//         }
//     }
// }

pub trait Overlaps<T> {
    fn overlaps(&self, v: T) -> bool;
}
impl Overlaps<i64> for Range<i64> {
    fn overlaps(&self, v: i64) -> bool {
        self.start <= v && v < self.end
    }
}
impl Overlaps<i64> for Range<Option<i64>> {
    fn overlaps(&self, v: i64) -> bool {
        match (self.start, self.end) {
            (Some(s), Some(e)) => s <= v && v < e,
            (Some(s), None) => s <= v,
            (None, Some(e)) => v < e,
            (None, None) => false,
        }
    }
}
impl Overlaps<Range<i64>> for Range<Option<i64>> {
    fn overlaps(&self, v: Range<i64>) -> bool {
        self.overlaps(v.start) || self.overlaps(v.end)
    }
}
