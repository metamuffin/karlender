use karlcommon::{Condition, Property, Schedule, Task};

use crate::TASKS;

pub fn load_demo() {
    TASKS.write().unwrap().insert(
        0,
        Task {
            id: 0,
            name: "Mittagessen im Februar".to_string(),
            description: None,
            tags: vec!["Essen".to_string(), "Unwichtig".to_string()],
            schedule: Schedule::Condition(Condition::And(vec![
                Condition::Equal {
                    modulus: None,
                    prop: Property::Monthofyear,
                    value: 1,
                },
                Condition::Equal {
                    modulus: None,
                    prop: Property::Hour,
                    value: 12,
                },
            ])),
        },
    );
    TASKS.write().unwrap().insert(
        1,
        Task {
            id: 1,
            name: "Abendessen oder Frühstück".to_string(),
            description: Some("Nom nom nom".to_string()),
            tags: vec!["Essen".to_string()],
            schedule: Schedule::Condition(Condition::Or(vec![
                Condition::Equal {
                    modulus: None,
                    prop: Property::Hour,
                    value: 18,
                },
                Condition::Equal {
                    modulus: None,
                    prop: Property::Hour,
                    value: 7,
                },
            ])),
        },
    );
    TASKS.write().unwrap().insert(
        2,
        Task {
            id: 2,
            description: None,
            name: "Wichtiger termin™".to_string(),
            tags: vec![],
            schedule: Schedule::Static(1654997366..1655007366),
        },
    );

    TASKS.write().unwrap().insert(
        3,
        Task {
            id: 3,
            description: None,
            name: "Staubsaugen".to_string(),
            tags: vec!["Unwichtig".to_string()],
            schedule: Schedule::Dynamic {
                scheduled: None,
                duration: 15 * 60,
                priority: 2.0,
                condition: Condition::Equal {
                    prop: Property::Monthofyear,
                    value: 6,
                    modulus: None,
                },
            },
        },
    );
    TASKS.write().unwrap().insert(
        4,
        Task {
            id: 4,
            description: Some("sollte ich wirklich mal machen".to_string()),
            name: "Geschirrspüler ausräumen".to_string(),
            tags: vec!["Unwichtig".to_string()],
            schedule: Schedule::Dynamic {
                scheduled: None,
                duration: 15 * 60,
                priority: 5.0,
                condition: Condition::Never,
            },
        },
    );
}
