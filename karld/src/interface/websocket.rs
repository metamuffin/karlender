use crate::handle_packet;
use crate::interface::generic;
use crossbeam_channel::{Receiver, Sender};
use karlcommon::{ClientboundPacket, ProtoError, ServerboundPacket};
use log::{debug, info};
use std::net::{TcpListener, TcpStream};
use std::thread;
use tungstenite::{accept, Message};

pub fn run() {
    info!("binding websocket server");
    let server = TcpListener::bind("127.0.0.1:9001").unwrap();
    info!("listening");
    for stream in server.incoming() {
        thread::spawn(move || generic::handle_connection(handle_connection, stream));
    }
}

fn handle_connection(
    id: u32,
    (responder, responses): (Sender<ClientboundPacket>, Receiver<ClientboundPacket>),
    stream: Result<TcpStream, std::io::Error>,
) -> anyhow::Result<()> {
    let stream = stream?;
    stream.set_nonblocking(true)?;
    let mut websocket = accept(stream)?;

    loop {
        match websocket.read_message() {
            Ok(Message::Text(t)) => {
                debug!("<-  {t:?}");
                match serde_json::from_str::<ServerboundPacket>(&t) {
                    Ok(packet) => {
                        handle_packet(id, packet, responder.clone())
                    },
                    Err(e) => {
                        responder.send(karlcommon::ClientboundPacket::Error(ProtoError::FormatError(format!("{e}"))))?
                    },
                }
            }
            Ok(_) => (),
            Err(tungstenite::Error::ConnectionClosed) => {
                break Ok(());
            }
            Err(tungstenite::Error::Io(e))
                if let std::io::ErrorKind::WouldBlock = e.kind() => {
                    // its fine
                }
            Err(e) => Err(e)?,
        }
        for r in responses.try_iter() {
            websocket.write_message(Message::Text(serde_json::to_string(&r)?))?;
        }
        thread::sleep(std::time::Duration::from_millis(50)); // how would you do this properly??
    }
}
