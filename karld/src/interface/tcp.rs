use crate::interface::generic;
use karlcommon::interfaces::tcp_addr;
use log::info;
use std::net::TcpListener;
use std::thread;

pub fn run() {
    info!("binding to socket");
    let listener = TcpListener::bind(tcp_addr()).unwrap();
    info!("listening.");
    loop {
        let (stream, _addr) = listener.accept().unwrap();
        thread::spawn(move || {
            generic::handle_connection(generic::stream, (stream.try_clone().unwrap(), stream))
        });
    }
}
