use super::generic;
use log::info;

pub fn run() {
    info!("reading packets from stdin");
    generic::handle_connection(generic::stream, (std::io::stdin(), std::io::stdout()));
}
