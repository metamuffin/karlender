use crate::interface::generic;
use karlcommon::interfaces::unix_path;
use log::info;
use std::os::unix::net::UnixListener;
use std::thread;

pub fn run() {
    if unix_path().exists() {
        info!("remove old socket");
        std::fs::remove_file(unix_path()).unwrap();
    }
    info!("binding to socket");
    let listener = UnixListener::bind(unix_path()).unwrap();
    info!("listening.");

    loop {
        let (stream, _addr) = listener.accept().unwrap();
        thread::spawn(move || {
            generic::handle_connection(generic::stream, (stream.try_clone().unwrap(), stream))
        });
    }
}
