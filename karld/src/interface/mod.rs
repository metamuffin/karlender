#[cfg(feature = "stdio")]
mod stdio;
#[cfg(feature = "tcp")]
mod tcp;
#[cfg(feature = "unix")]
mod unix;
#[cfg(feature = "websocket")]
mod websocket;

pub mod generic;

pub fn start() {
    #[cfg(feature = "unix")]
    std::thread::spawn(|| unix::run());
    #[cfg(feature = "websocket")]
    std::thread::spawn(|| websocket::run());
    #[cfg(feature = "stdio")]
    std::thread::spawn(|| stdio::run());
    #[cfg(feature = "tcp")]
    std::thread::spawn(|| tcp::run());

    #[cfg(not(feature = "websocket"))]
    #[cfg(not(feature = "unix"))]
    #[cfg(not(feature = "tcp"))]
    #[cfg(not(feature = "stdio"))]
    log::warn!("no interfaces enabled, daemon will be inaccesssible")
}
