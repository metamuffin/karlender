use karlcommon::Task;
use log::{error, info};

use crate::TASKS;
use std::{
    collections::BTreeMap,
    fs::{create_dir_all, File},
    io,
    path::{Path, PathBuf},
};

fn save_dir() -> PathBuf {
    std::env::var("XDG_DATA_HOME")
        .map(|p| Path::new(p.as_str()).to_path_buf())
        .unwrap_or_else(|_| {
            Path::new(&std::env::var("HOME").expect("$HOME needs to be set"))
                .to_path_buf()
                .join(".local")
                .join("share")
        })
        .join("karl")
}

pub fn save() {
    if let Err(r) = save_inner() {
        error!("save failed: {}", r);
    }
}

pub fn save_inner() -> io::Result<()> {
    info!("saving...");
    create_dir_all(save_dir())?;
    let file = File::create(save_dir().join("tasks"))?;
    serde_json::to_writer(file, &TASKS.read().unwrap().to_owned())?;
    info!("done");
    Ok(())
}

pub fn load() -> io::Result<()> {
    info!("loading savestate...");
    let file = File::open(save_dir().join("tasks"))?;
    let tasks: BTreeMap<u64, Task> = serde_json::from_reader(file)?;

    let mut guard = TASKS.write().unwrap();
    guard.clear();
    guard.extend(tasks.into_iter());
    info!("done");

    Ok(())
}
