#![feature(box_syntax)]
#![feature(fs_try_exists)]
#![feature(if_let_guard)]

pub mod condition;
pub mod demo;
pub mod helper;
pub mod interface;
pub mod savestate;
pub mod schedule;

use crate::schedule::schedule_dynamic;
use chrono::NaiveDateTime;
use condition::ConditionFind;
use crossbeam_channel::Sender;
use helper::Overlaps;
use karlcommon::{ClientboundPacket, ProtoError, Schedule, ServerboundPacket, Task};
use log::{debug, error, info};
use std::{
    collections::HashMap,
    sync::{atomic::AtomicU32, RwLock},
};

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_env("LOG")
        .init();
    info!("logging");
    if let Err(e) = savestate::load() {
        error!("load failed: {}", e);
    }
    #[cfg(target_os = "windows")]
    log::warn!("Windows is not supported, please do not report any bugs for this platform. Use a free operating system instead.");
    #[cfg(target_os = "macos")]
    log::warn!("Mac OS is not supported, please do not report any bugs for this platform. Use a free operating system instead.");

    std::thread::spawn(move || {
        std::thread::sleep(std::time::Duration::from_secs_f64(0.1));
        schedule_dynamic();
    });

    // demo::load_demo();

    interface::start();
    loop {
        std::thread::sleep(std::time::Duration::from_secs_f64(100.0));
    }
}

lazy_static::lazy_static! {
    static ref TASKS: RwLock<HashMap<u64, Task>> = RwLock::new(HashMap::new());
    static ref CLIENTS: RwLock<HashMap<u32, Sender<ClientboundPacket>>> = RwLock::new(HashMap::new());
    static ref CLIENT_ID_COUNTER: AtomicU32 = AtomicU32::new(0);
}

fn broadcast_invalidation() {
    CLIENTS
        .write()
        .unwrap()
        .values()
        .for_each(|r| drop(r.send(ClientboundPacket::InvalidateState)));
}

pub fn handle_packet(client: u32, packet: ServerboundPacket, responder: Sender<ClientboundPacket>) {
    // std::thread::sleep(std::time::Duration::from_millis(75)); // for testing clients with latency
    match packet {
        ServerboundPacket::Sync => {
            drop(responder.send(ClientboundPacket::Sync));
        }
        ServerboundPacket::ListTasks => {
            drop(responder.send(ClientboundPacket::TaskList(
                TASKS.read().unwrap().values().map(|e| e.clone()).collect(),
            )));
        }
        ServerboundPacket::UpdateTask(t) => {
            TASKS.write().unwrap().insert(t.id, t);
            broadcast_invalidation();
            savestate::save();
        }
        ServerboundPacket::RemoveTask(i) => {
            if TASKS.write().unwrap().remove(&i).is_some() {
                broadcast_invalidation();
            } else {
                drop(responder.send(ClientboundPacket::Error(ProtoError::UnknownTask)));
            }
            savestate::save();
        }
        ServerboundPacket::Handshake { version } => {
            debug!("{client}: version {version}");
        }
        ServerboundPacket::ListInstances { range, task, limit } => {
            let t = match TASKS.read().unwrap().get(&task).cloned() {
                Some(t) => t,
                None => {
                    return drop(responder.send(ClientboundPacket::Error(ProtoError::UnknownTask)))
                }
            };

            let mut ocs = vec![];
            match t.schedule {
                Schedule::Never => (),
                Schedule::Dynamic { .. } => (), // TODO
                Schedule::Static(r) => {
                    if range.overlaps(r.clone()) {
                        ocs.push(Some(r.start)..Some(r.end))
                    }
                }
                Schedule::Condition(o) => {
                    let mut time =
                        NaiveDateTime::from_timestamp_opt(range.start.unwrap_or(0), 0).unwrap();
                    let end_time = range
                        .end
                        .map(|e| NaiveDateTime::from_timestamp_opt(e, 0).unwrap());
                    for _ in 0..limit {
                        let start =
                            o.find(condition::Edge::Start, condition::Direction::Forward, time);
                        let end = o.find(condition::Edge::End, condition::Direction::Forward, time);
                        ocs.push(start.map(|e| e.timestamp())..end.map(|e| e.timestamp()));
                        if let Some(s) = end {
                            if let Some(e) = end_time {
                                if s > e {
                                    break;
                                }
                            }
                            time = s;
                        } else {
                            break;
                        }
                    }
                }
            }
            drop(responder.send(ClientboundPacket::InstanceList(ocs)));
        }
    }
}
