use karlcommon::Condition;

pub fn indent(s: &str) -> String {
    s.replace("\n", "\n  ")
}

pub fn fmt_condition(c: &Condition) -> String {
    match c {
        Condition::Never => format!("(never)"),
        Condition::From(_) => todo!(),
        Condition::Or(cs) => cs
            .iter()
            .map(|e| format!("{{ {} }}", fmt_condition(e)))
            .reduce(|a, b| format!("{} ∨ {}", a, b))
            .unwrap_or("never".to_string()),
        Condition::And(cs) => cs
            .iter()
            .map(|e| format!("{{ {} }}", fmt_condition(e)))
            .reduce(|a, b| format!("{} ∧ {}", a, b))
            .unwrap_or("never".to_string()),
        Condition::Invert(_) => todo!(),
        Condition::Equal {
            prop,
            value,
            modulus,
        } => {
            if let Some(m) = modulus {
                format!("{:?} ≡ {} (mod {})", prop, value, m)
            } else {
                format!("{:?} = {}", prop, value)
            }
        }
        Condition::Range {
            prop,
            min,
            max,
            modulus,
        } => {
            if let Some(m) = modulus {
                format!("{} < {:?} < {} (mod {})", min, prop, max, m)
            } else {
                format!("{} < {:?} < {}", min, prop, max)
            }
        }
    }
}
