use crossbeam_channel::Receiver;
use karlcommon::{version, ClientboundPacket, ServerboundPacket};
use log::{debug, error};
use std::{
    io::{BufRead, BufReader, Write},
    os::unix::net::UnixStream,
    thread,
};

pub struct Client {
    socket: UnixStream,
    pub receiver: Receiver<ClientboundPacket>,
}

impl Client {
    pub fn new(socket: UnixStream) -> Self {
        let (sender, receiver) = crossbeam_channel::unbounded();
        let mut reader = BufReader::new(socket.try_clone().unwrap());
        thread::spawn(move || loop {
            let mut buf = String::new();
            reader.read_line(&mut buf).unwrap();
            let p: ClientboundPacket = serde_json::from_str(buf.as_str()).unwrap();
            debug!("<- {:?}", p);
            if let ClientboundPacket::Error(e) = p {
                error!("daemon reported error: {:?}", e);
            } else {
                sender.send(p).unwrap();
            }
        });
        let mut c = Self { receiver, socket };
        c.send(ServerboundPacket::Handshake {
            version: version!(),
        });
        return c;
    }

    pub fn send(&mut self, p: ServerboundPacket) {
        debug!("-> {:?}", p);
        self.socket
            .write_fmt(format_args!("{}\n", serde_json::to_string(&p).unwrap()))
            .unwrap()
    }
}
