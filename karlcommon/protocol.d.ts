
export type ServerboundPacket = Sync | Handshake | ListTasks | ListInstances | UpdateTask | RemoveTask
export type ClientboundPacket = Handshake | Error | TaskList | InstanceList | Sync | InvalidateState

export interface Sync { type: "sync", data: null }
export interface Error { type: "error", data: { kind: "unknown_task", details: null } | { kind: "format_error", details: string } }
export interface Handshake { type: "handshake", data: { version: string } }
export interface ListTasks { type: "list_tasks", data: null }
export interface TaskList { type: "task_list", data: Task[] }
export interface ListInstances { type: "list_instances", data: { task: number, range: Range } }
export interface InstanceList { type: "instance_list", data: Range[] }
export interface UpdateTask { type: "update_task", data: Task }
export interface RemoveTask { type: "remove_task", data: number }
export interface InvalidateState { type: "invalidate_state", data: null }

export interface Range { start?: number, end?: number }

export interface Task {
    id: number
    name: string,
    description: string,
    tags: string[],
    schedule: Schedule
}

export type Schedule = { type: "never" }
    | { type: "static", options: Range }
    | { type: "condition", options: Condition }
    | { type: "dynamic", options: { priority: number, scheduled?: number, duration: number, deadline?: Condition } }

export type Condition = { from?: Condition }
    | { or?: Condition[] }
    | { and?: Condition[] }
    | { equal?: { prop: Thing, value: number, mod?: number } }
    | { range?: { prop: Thing, min: number, max: number, mod?: number } }

export type Thing = "year"
    | "monthofyear"
    | "weekofmonth"
    | "dayofyear"
    | "dayofmonth"
    | "dayofweek"
    | "hour"
    | "minute"
    | "second"
    | "unix"

/*
    examples:

    11:00 - 12:00 every first monday of the month

    and: [
        { range: { prop: "hour", min: 11, max: 12 } },
        { equal: { prop: "dayofweek", value: 0 } },
        { equal: { prop: "weekofmonth", value: 0 } }
    ]

*/
