use crate::{Property, Task};

impl Property {
    pub const VALUES: &'static [Property] = &[
        Property::Year,
        Property::Monthofyear,
        Property::Weekofmonth,
        Property::Dayofyear,
        Property::Dayofmonth,
        Property::Dayofweek,
        Property::Hour,
        Property::Minute,
        Property::Second,
        Property::Unix,
    ];
    pub fn to_str(self) -> &'static str {
        match self {
            Property::Year => "Year",
            Property::Monthofyear => "Month of the year",
            Property::Weekofmonth => "Week of the month",
            Property::Dayofyear => "Day of the year",
            Property::Dayofmonth => "Day of the month",
            Property::Dayofweek => "Day of the week",
            Property::Hour => "Hour",
            Property::Minute => "Minute",
            Property::Second => "Second",
            Property::Unix => "Unix timestamp",
        }
    }
}

impl Task {
    pub fn default_with_id(id: u64) -> Self {
        Self {
            id,
            name: Default::default(),
            description: Default::default(),
            tags: Default::default(),
            schedule: crate::Schedule::Never,
        }
    }
}
