pub mod protocol;
pub use protocol::*;
pub mod misc;

#[macro_export]
macro_rules! version {
    () => {
        format!(
            "{} {} (lib{})",
            env!("CARGO_PKG_NAME"),
            env!("CARGO_PKG_VERSION"),
            karlcommon::own_version()
        )
    };
}

pub fn own_version() -> String {
    format!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
}

pub mod interfaces {
    use std::net::{Ipv4Addr, SocketAddr};
    use std::os::unix::prelude::MetadataExt;
    use std::path::{Path, PathBuf};

    fn getuid() -> u32 {
        std::fs::metadata("/proc/self").unwrap().uid()
    }
    pub fn unix_path() -> PathBuf {
        std::env::var("XDG_RUNTIME_DIR")
            .map(|p| Path::new(p.as_str()).to_path_buf())
            .unwrap_or_else(|_| Path::new("/run/user").join(format!("{}", getuid())))
            .join("karlender")
    }
    pub fn websocket_addr() -> SocketAddr {
        SocketAddr::new(std::net::IpAddr::V4(Ipv4Addr::LOCALHOST), 18752)
    }
    pub fn tcp_addr() -> SocketAddr {
        SocketAddr::new(std::net::IpAddr::V4(Ipv4Addr::LOCALHOST), 18751)
    }
}
