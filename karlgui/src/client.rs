use crossbeam_channel::Receiver;
use karlcommon::{interfaces::unix_path, version, ClientboundPacket, ServerboundPacket};
use log::{debug, error};
use std::{
    io::{BufRead, BufReader, Write},
    os::unix::net::UnixStream,
    process::exit,
    thread,
};

pub struct Client {
    socket: UnixStream,
    pub busy: bool,
    pub receiver: Receiver<ClientboundPacket>,
}

impl Client {
    pub fn new() -> Self {
        let socket = match UnixStream::connect(unix_path()) {
            Ok(s) => s,
            Err(e) => {
                error!("failed to connect to socket: {}", e);
                error!("make sure that karld is running");
                exit(1)
            }
        };

        let (sender, receiver) = crossbeam_channel::unbounded();
        let mut reader = BufReader::new(socket.try_clone().unwrap());
        thread::spawn(move || loop {
            let mut buf = String::new();
            reader.read_line(&mut buf).unwrap();
            let p: ClientboundPacket = serde_json::from_str(buf.as_str()).unwrap();
            debug!("<- {:?}", p);
            if let ClientboundPacket::Error(e) = p {
                error!("daemon reported error: {:?}", e);
            } else {
                sender.send(p).unwrap();
            }
        });
        let mut c = Self {
            receiver,
            socket,
            busy: true,
        };
        c.send(ServerboundPacket::Handshake {
            version: version!(),
        });
        c.send(ServerboundPacket::Sync);
        return c;
    }

    pub fn send(&mut self, p: ServerboundPacket) {
        debug!("-> {:?}", p);
        self.socket
            .write_fmt(format_args!("{}\n", serde_json::to_string(&p).unwrap()))
            .unwrap()
    }

    pub fn send_sync(&mut self, p: ServerboundPacket) {
        self.busy = true;
        self.send(p);
        self.send(ServerboundPacket::Sync);
    }
}
