pub mod client;
pub mod globals;
pub mod helper;
pub mod views;

use crate::{client::Client, globals::Globals};
use eframe::CreationContext;
use egui::CentralPanel;
use karlcommon::ServerboundPacket;
use log::info;
use views::{calendar::Calendar, edit::ShowAndEdit};

fn main() {
    env_logger::builder()
        .filter_level(log::LevelFilter::Info)
        .parse_env("LOG")
        .init();
    info!("starting native app");
    eframe::run_native(
        "karlender",
        eframe::NativeOptions::default(),
        Box::new(move |cc| Box::new(App::new(cc))),
    )
}

struct App {
    g: Globals,

    current_tab: Tab,

    show_and_edit: ShowAndEdit,
    calendar: Calendar,
}

#[derive(PartialEq)]
enum Tab {
    ShowAndEdit,
    CalendarWeek,
}

impl App {
    pub fn new(cc: &CreationContext) -> Self {
        info!("app creation");
        cc.egui_ctx.set_visuals(egui::Visuals::dark());
        let mut client = Client::new();
        info!("connected");
        client.send(ServerboundPacket::ListTasks);
        App {
            current_tab: Tab::CalendarWeek,
            g: Globals::new(client),
            show_and_edit: Default::default(),
            calendar: Default::default(),
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.g.update_network();
        CentralPanel::default().show(ctx, |ui| {
            ui.add_enabled_ui(!self.g.client.busy, |ui| {
                ui.horizontal(|ui| {
                    ui.selectable_value(&mut self.current_tab, Tab::ShowAndEdit, "Tasks");
                    ui.selectable_value(&mut self.current_tab, Tab::CalendarWeek, "Calendar: Week");
                });
                ui.separator();
                match self.current_tab {
                    Tab::ShowAndEdit => self.show_and_edit.ui(ui, &mut self.g),
                    Tab::CalendarWeek => self.calendar.ui(ui, &mut self.g),
                }
            });
        });
    }
}
