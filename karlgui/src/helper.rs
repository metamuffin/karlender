use chrono::NaiveDateTime;
use egui::{DragValue, Ui};
use karlcommon::Property;

pub fn from_timestamp(t: i64) -> NaiveDateTime {
    NaiveDateTime::from_timestamp_opt(t, 0).unwrap()
}

pub fn format_value(prop: Property, value: i64) -> String {
    match prop {
        Property::Year => format!("{value}"),
        Property::Monthofyear => format!("{}", month_to_str(value)),
        Property::Weekofmonth => format!("{value}"),
        Property::Dayofyear => format!("{value}"),
        Property::Dayofmonth => format!("{value}"),
        Property::Dayofweek => format!("{}", weekday_to_str(value)),
        Property::Hour => format!("{value}h"),
        Property::Minute => format!("{value}min"),
        Property::Second => format!("{value}s"),
        Property::Unix => format!("{value}s"),
    }
}

pub fn edit_value(ui: &mut Ui, prop: Property, value: &mut i64) {
    match prop {
        Property::Year => {
            ui.add(DragValue::new(value));
        }
        Property::Monthofyear => {
            egui::ComboBox::from_id_source(ui.id())
                .selected_text(format_value(prop, *value))
                .show_ui(ui, |ui| {
                    for v in 0..12 {
                        ui.selectable_value(value, v, format_value(prop, v));
                    }
                });
        }
        Property::Dayofweek => {
            egui::ComboBox::from_id_source(ui.id())
                .selected_text(format_value(prop, *value))
                .show_ui(ui, |ui| {
                    for v in 0..7 {
                        ui.selectable_value(value, v, format_value(prop, v));
                    }
                });
        }
        Property::Weekofmonth => {
            ui.add(DragValue::new(value).clamp_range(0..=5));
        }
        Property::Dayofyear => {
            ui.add(DragValue::new(value).clamp_range(0..=366));
        }
        Property::Dayofmonth => {
            ui.add(DragValue::new(value).clamp_range(0..=31));
        }
        Property::Hour => {
            ui.add(DragValue::new(value).clamp_range(0..=24).suffix("h"));
        }
        Property::Minute => {
            ui.add(DragValue::new(value).clamp_range(0..=60).suffix("m"));
        }
        Property::Second => {
            ui.add(DragValue::new(value).clamp_range(0..=60).suffix("s"));
        }
        Property::Unix => {
            ui.add(DragValue::new(value).suffix("s"));
        }
    }
}

pub fn weekday_to_str(value: i64) -> &'static str {
    match value {
        0 => "Monday",
        1 => "Thuesday",
        2 => "Wednesday",
        3 => "Thursday",
        4 => "Friday",
        5 => "Saturday",
        6 => "Sunday",
        _ => "(invalid)",
    }
}
pub fn month_to_str(value: i64) -> &'static str {
    match value {
        0 => "January",
        1 => "February",
        2 => "March",
        3 => "April",
        4 => "May",
        5 => "June",
        6 => "July",
        7 => "August",
        8 => "September",
        9 => "October",
        10 => "November",
        11 => "December",
        _ => "(invalid)",
    }
}
pub fn ordering_suffix(value: u32) -> &'static str {
    match value {
        1 => "st",
        2 => "nd",
        _ => "th",
    }
}
